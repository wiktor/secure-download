use trust_dns_client::rr::{RData, RecordType};

use sequoia_openpgp as openpgp;

use openpgp::cert::prelude::*;
use openpgp::parse::{Parse, stream::*};
use openpgp::policy::StandardPolicy as P;

use anyhow;

use hyper::Client as HyperClient;
use hyper_tls::HttpsConnector;

use trust_dns_resolver::TokioAsyncResolver;
use trust_dns_resolver::config::*;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 {
	return Err(anyhow::anyhow!("First argument needs to be a URL pointing to the signed file.").into());
    }

    let target: hyper::Uri = args[1].parse()?;

    let mut opts = ResolverOpts::default();
    opts.validate = true;

    let config = ResolverConfig::quad9();
    
    let resolver = TokioAsyncResolver::tokio(config, opts)?;

    eprintln!("secure-download: Retrieving authorized fingerprints for domain {}.", target.host().unwrap());
    
    let answers = resolver.lookup(format!("{}.", target.host().unwrap()), RecordType::TXT, Default::default()).await?;

    let fingerprints = answers.iter().flat_map(|answer| {
	if let &RData::TXT(ref txt) = answer {
	    Some(format!("{}", txt))
	} else {
	    None
	}
    }).filter(|text| {
	text.to_ascii_lowercase().starts_with("openpgp4fpr:")
    }).map(|text| {
	let mut split = text.split(':');
	split.next();
	split.next().unwrap().parse().unwrap()
    }).collect::<Vec<openpgp::Fingerprint>>();
    fingerprints.iter().for_each(|fpr| eprintln!("secure-download: Authorized key: {}", fpr));

    let mut certs = vec![];

    let client = HyperClient::builder()
	.pool_idle_timeout(std::time::Duration::from_secs(30))
        .build::<_, hyper::Body>(HttpsConnector::new());

    for fpr in &fingerprints {
	let res = client.get(format!("https://keys.openpgp.org/vks/v1/by-fingerprint/{:X}", fpr).parse()?).await?;
	eprintln!("secure-download: {} {}", fpr, res.status());
	let bytes = hyper::body::to_bytes(res.into_body()).await?;

        let parser = CertParser::from_bytes(&bytes);
	if let Ok(parser) = parser {
            for cert in parser {
		match cert {
                    Ok(cert) => {
			certs.push(cert);
                    }
                    Err(_) => (),
		}
            }
	}
    }

    eprintln!("secure-download: Retreived {} certificate(s).", certs.len());

    let helper = Helper {
	authorized_certs: certs.into_iter().filter(|cert|
           fingerprints.contains(&cert.fingerprint())).collect::<Vec<_>>(),
    };

    let ref policy = P::new();

    let res = client.get(target).await?;
    eprintln!("secure-download: target {}", res.status());
    let bytes = hyper::body::to_bytes(res.into_body()).await?;
    
    let mut verifier = VerifierBuilder::from_bytes(&bytes)?
        .with_policy(policy, None, helper)?;

    std::io::copy(&mut verifier, &mut std::io::stdout())?;
    
    Ok(())
}

struct Helper {
    authorized_certs: Vec<Cert>
}

impl<'a> VerificationHelper for Helper {
    fn get_certs(&mut self, _ids: &[openpgp::KeyHandle])
                 -> openpgp::Result<Vec<openpgp::Cert>> {
         Ok(self.authorized_certs.clone())
    }
 
    fn check(&mut self, structure: MessageStructure)
             -> openpgp::Result<()> {
        // In this function, we implement our signature verification
        // policy.
 
        let mut good = false;
        for (i, layer) in structure.into_iter().enumerate() {
            match (i, layer) {
                // First, we are interested in signatures over the
                // data, i.e. level 0 signatures.
                (0, MessageLayer::SignatureGroup { results }) => {
                    // Finally, given a VerificationResult, which only says
                    // whether the signature checks out mathematically, we apply
                    // our policy.
                    match results.into_iter().next() {
                        Some(Ok(ref gc)) => {
                            eprintln!("secure-download: Certificate: {:X}", gc.ka.cert().fingerprint());
                            eprintln!("secure-download: Signing key: {:X}", gc.ka.fingerprint());
                            if let Some(date) = gc.sig.signature_creation_time() {
                                use chrono::prelude::{DateTime, Local};
                                let dt: DateTime<Local> = date.into();
                                eprintln!("secure-download: Signing date: {}", dt.format("%Y-%m-%d %H:%M:%S"));
                            }
                            let cert = gc
                                .ka
                                .cert()
                                .clone()
                                .with_policy(gc.ka.policy(), None)?
                                .primary_userid()?;
                            let user_id = String::from_utf8_lossy(cert.value());
                            eprintln!("secure-download: Identity: {}", user_id);
                        good = true}
		    ,
                        Some(Err(e)) =>
                            return Err(openpgp::Error::from(e).into()),
                        None =>
                            return Err(anyhow::anyhow!("No signature")),
                    }
                },
		(0, MessageLayer::Compression { .. }) => {
		    return Err(anyhow::anyhow!(
			"Compressed content not supported. Please use `gpg --compression none --sign ...`"));
		}
                _ => return Err(anyhow::anyhow!(
                    "Unexpected message structure")),
            }
        }
 
        if good {
            Ok(()) // Good signature.
        } else {
            Err(anyhow::anyhow!("Signature verification failed"))
        }
    }
}
