# Secure download

A proof of concept of a secure download tool.

What does it do?

1. You run it with a URL pointing to a signed file:

    `cargo run -- https://metacode.biz/sandbox/signed-content.txt.gpg`

2. It fetches authorized OpenPGP certificate fingerprints from DNSSEC signed domain's TXT records:

    `dig TXT metacode.biz +dnssec +multi`

Note that validation of DNSSEC may be slow (it's about 10 seconds on my computer).

3. Then it fetches certificates for these fingerprints from `keys.openpgp.org` (the actual source of the certificates is not relevant but `k.o.o` is used as a quick way to discover keys by fingerprints).

4. Then it downloads the signed target file, verifies the signature using certificates from the previous step.

5. If validation succeeds the content of the signed file is written to stdout:

```
$ cargo run -- https://metacode.biz/sandbox/signed-content.txt.gpg
   Compiling secure-download v0.1.0 (secure-download)
    Finished dev [unoptimized + debuginfo] target(s) in 4.50s
     Running `target/debug/secure-download 'https://metacode.biz/sandbox/signed-content.txt.gpg'`
secure-download: Retrieving authorized fingerprints for domain metacode.biz.
secure-download: Authorized key: 653909A2F0E37C106F5FAF546C8857E0D8E8F074
secure-download: Authorized key: 198C722A4BAC336E9DAAAE44579D01B3ABE1540E
secure-download: 653909A2F0E37C106F5FAF546C8857E0D8E8F074 200 OK
secure-download: 198C722A4BAC336E9DAAAE44579D01B3ABE1540E 404 Not Found
secure-download: Retreived 1 certificate(s).
secure-download: target 200 OK
secure-download: Certificate: 653909A2F0E37C106F5FAF546C8857E0D8E8F074
secure-download: Signing key: E7E2B84A36457BEA3F43692DE68BE3B312FA33FC
secure-download: Signing date: 2021-05-24 10:14:23
secure-download: Identity: Wiktor Kwapisiewicz
Hello,

It works!

This is a signed content.
```

## Preparing signed file

Signed file must be embedded with no compression (current limitation):

    gpg --compression none --sign signed-content.txt

## DNSSEC

Make sure that your DNSSEC setup works correctly by using https://dnsviz.net/

## Caveats

The following list limitations of the current proof of concept:

* signature must be created with no compression.
* signing keys need to be present on `keys.openpgp.org`.
